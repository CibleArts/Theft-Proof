extends MultiplayerSpawner

func _init():
	spawn_function = _spawn_rocket

func _spawn_rocket(data):
#	if data.size() != 2 or typeof(data[0]) != TYPE_VECTOR2 or typeof(data[2]) != TYPE_INT:
#		return null
	var rocket = preload("res://rocket.tscn").instantiate()
#	print(200 * rocket.global_position.direction_to(data[0]))
	rocket.position = data[0]
	rocket.rotation = data[1]
	rocket.from_player = data[2]
#	print(data[0], " ", data[1], " ", data[2])
#	rocket.linear_velocity = data[0]
#	rocket.apply_central_impulse(data[0])
#	rocket.apply_impulse(Vector2(500.0, -20.0), data[0])
#	rocket.apply_impulse(200 * rocket.global_position.direction_to(data[0]), data[0])
	return rocket
