# Theft Proof

## Logline

Sam and Leah’s home had been invaded by their school bullies. Sam and Leah fight back the bullies and even more shocking villains to get their home back.

## Genre

A co-op, narrative driven, action, puzzle. Inspired by “It-takes-two,” but stripped down to a 2d, indie game with a more kids friendly story. The game is built around the basic single mechanic where there is only one bullet which bounces around. Player has to collect it to fire again.

## Synopsis

### ACT 1

Samuel David Jr, that was the name on the certificate hanging on the wall, the photo of his graduation celebration with his parents and sister Leah was framed and kept right next to their bed. It was dark and the rain poured outside. Sam could hear the sounds of every hit, the howl of wind and voice of people climbing into the house. He could sense there would be about seven to nine of them. Sam quickly wakes in his bed as the noises start rapidly moving around.

The voices sounded frighteningly familiar, he tried to remember each one of them.

Sam decides to get off from the bed, pushes the cot and pulls out a long deadly bazooka from under the cot. He then picks another bazooka and hands it to Leah, who was already awake just like him. He grabs a boxing glove hanging on top, and tapes it with the rocket of bazooka.

[Cutscene] The bullies hide around the room and wait for the members to step out.

But to their terrific shock, it was Sam who emerged from the door followed by his sister Leah…

They quickly hide behind a table on either end as the bullies start a mad firing. Sam stares straight into the door they came from. Their commotion suddenly stops as they start to reload their weapons. Sam quickly uses the opportunity and fires the gloves towards a bully which quickly hits and breaks his nose, returning towards Leah, which she quickly catches and reloads her bazooka with and fires towards another bully aiming on Sam.

After defeating all three bullies they discover that the door is closed from outside. They somehow find a way to escape and proceed to the next room.

### Act 2

Sam and Leah reached the corridor, jumped through the puzzled platform and got to their parents bedroom door, which was unfortunately locked. Sam and Leah decided to take the long path, they found the nearest door unlocked and proceeded through it, which was a kitchen and the bullies had made a huge preparation and hidden in it. They passed them all and climbed to the attic which led to the bedroom.

Sam and Leah passed all the jammed pile and got through the attic until all of sudden the floor broke and Leah fell into the luxurious bathroom, Sam quickly jumped in as a group of bullies hiding, started to attack. They defeated them all and realized that the bathroom door led directly to the bedroom. 

Sam and Leah quickly smashed through the door to find out that their parents were missing.

Sam and Leah found out that their parents had been kidnapped by the bullies, they upgraded themselves with the gears available in the bedroom and prepared themselves for the rescue Mission. 

Sam and Leah entered the drawing room, through the bedroom door and fought all the bullies with their new upgrade. They heroically opened the front door to find the terror, the house had been surrounded by a huge trap made by the bullies. 

They passed all the obstacles and reached a bullies camp where they fought a group of bullies and found a tunnel through which the bullies entered and there was a bigger camp beyond the tunnels where they could have taken the parents.

Sam and Leah passed the tunnels, defeated some bullies on the way and reached the camp which was completely empty with an audio tape, through which they found out that the house had been successfully captured.

### ACT 3

Sam and Leah upgraded their armor and tracked the long path through which they came to the camp…

As they entered the house, yellow gas evicted from the house, from the gas appeared the parents armed in skeleton suits and covered in poison. As soon as they notice Sam and Leah they start the attack.

On the edge of defeat Sam and Leah defeated the parents and rose in their bed awake, they stared at the wall for a minute and then went back to sleep.

## Characters

### Sam

Sam is a brave, sensitive and extremely talented kid who’s really smart and quick in his instincts.

### Leah

Leah is an equally brave younger sister of Sam, who occasionally makes mistakes because of her age but still a very powerful companion when it comes to solving puzzles and fighting bullies.

## Music

As the game takes place in the night, nice calm music may work better which rises up when enemies come into frame, the bigger the risk, the tenser the music, the pace adapts based on the visuals based on the change in pixels. Some satisfying sound effects, when shooting and catching back the bullet may make the game better.

## Visuals

More bright colors are chosen for the characters, including the enemies to pop up from the background. Unsaturated or less contrast background will make this design better (still experimenting).

## Flow

![](flow.png)
