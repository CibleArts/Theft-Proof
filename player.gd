extends CharacterBody2D

const MOTION_SPEED = 90.0
const BOMB_RATE = 0.5

@export
var synced_position := Vector2()

@export
var stunned = false

@onready
var inputs = $Inputs
var last_bomb_time = BOMB_RATE

var rocket = preload("res://rocket.tscn")

var fire = true

func _ready():
	if $MultiplayerSynchronizer.get_multiplayer_authority() != multiplayer.get_unique_id():
		$Camera2D.make_current()
	
	stunned = false
	position = synced_position
	if str(name).is_valid_int():
		get_node("Inputs/InputsSync").set_multiplayer_authority(str(name).to_int())


func _physics_process(delta):
	$Rocket.look_at(get_global_mouse_position())
	
	if multiplayer.multiplayer_peer == null or str(multiplayer.get_unique_id()) == str(name):
		# The client which this player represent will update the controls state, and notify it to everyone.
		inputs.update()

	if multiplayer.multiplayer_peer == null or is_multiplayer_authority():
		# The server updates the position that will be notified to the clients.
		synced_position = position
		# And increase the bomb cooldown spawning one if the client wants to.
		last_bomb_time += delta
#		if not stunned and is_multiplayer_authority() and inputs.bombing and last_bomb_time >= BOMB_RATE:
#			last_bomb_time = 0.0
#			get_node("../../BombSpawner").spawn([position, str(name).to_int()])
		if is_multiplayer_authority() and $Inputs.bombing and fire:
			get_node("../../RocketSpawner").spawn([$Rocket/RocketPoint.get_global_position(), get_global_mouse_position().angle_to_point(position), str(name).to_int()])
#			$Sprite.play("empty")
			fire = false
	else:
		# The client simply updates the position to the last known one.
		position = synced_position

	if not stunned:
		# Everybody runs physics. I.e. clients tries to predict where they will be during the next frame.
		velocity = inputs.motion * MOTION_SPEED
		move_and_slide()


func set_player_name(value):
	get_node("label").text = value


@rpc("call_local")
func exploded(_by_who):
	if stunned:
		return
	stunned = true

func _on_area_2d_body_entered(body):
	if body.name == "rocket":
		fire = true
#		$Sprite.play("default")
		body.queue_free()
